# FYS5555 machine learning hands-on 

In this hands-on session you'll have the opportunity to apply two of the ML algorithm types most commonly associated with experimental particle physics to ATLAS data. The algorithms you'll work with are *boosted decision trees* and *neural networks*. After applying these algorithms to a simplified dataset used in the 2014 Higgs Machine Learning Challenge, you'll be able to take on some more complicated datasets from the ATLAS Open Data. In both cases you'll be trying to separate signal events from background events, that is, you'll be doing *classification*.

Follow the links below to the two parts, starting with the boosted decision trees. Don't hesitate to ask if you have question!

* Boosted Decision Trees tutorial
* Neural Networks tutorial
* Applying ML to Open Data

## Package installation

If you're working on the Oslo Galaxy service, you won't have to install anything and you can ignore this section. If you're working independently on your own computer you'll need to install some packages, guidelines for which are as follows.

The easiest way to get started is to use a Conda virtual environment, which alows you to install everything in a self-contained environment which won't interfere with anything else in your system. First you should install [Anaconda](https://www.anaconda.com). 

Next, create a Conda virtual environment and launch it:

```
source $HOME/opt/anaconda3/bin/activate  
conda  create --name fys5555 tensorflow
conda activate fys5555
```

Now, install the following packages by typing the commands listed:


* [Jupyter Notebooks](https://anaconda.org/anaconda/notebook) (to enable coding via web-based notebooks):  
`conda install -c anaconda notebook`
* [Matplotlib](https://anaconda.org/conda-forge/matplotlib) (for drawing plots):  
`conda install -c conda-forge matplotlib`
* [GraphViz](https://anaconda.org/conda-forge/python-graphviz) (for viewing decision trees and neural network architectures):
```
conda install -c conda-forge python-graphviz; 
conda install -c anaconda graphviz
```
* [SciKitLearn](http://scikit-learn.org/stable/) (general machine learning framework):  
`conda install -c anaconda scikit-learn`
* [Pandas](https://anaconda.org/anaconda/pandas) (for dataframes):  
`conda install -c anaconda pandas`
* [XGBoost](https://anaconda.org/conda-forge/xgboost) (boosted decision trees):  
`conda install -c conda-forge xgboost`  
* [Keras](https://anaconda.org/conda-forge/keras) (for neural network support):  
`conda install -c conda-forge keras`

